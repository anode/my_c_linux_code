/*
 * @Author: itxiaoye
 * @Date: 2022-07-24 18:05:04
 * @LastEditors: Anode you@example.com
 * @LastEditTime: 2022-08-10 22:42:15
 * @FilePath: /program/src/drives/itxiaoye_led/ItxiaoyeLed.h
 * @Description: led驱动程序
 */
#ifndef __ITXIAOYE_LED_H__
#define __ITXIAOYE_LED_H__

/*****************************************************************************
* Include
*****************************************************************************/
#include "GlobalDataType.h"


/*******************************************************************************
* Macro
*******************************************************************************/
#define ITXIAOYE_LED_IOC_MAGIC  'l' // 定义幻数
#define ITXIAOYE_LED_IOC_LED_CONTR _IOW(ITXIAOYE_LED_IOC_MAGIC,itxiaoyeLedIocLedControl,int) // LED控制


/*******************************************************************************
* Constant
*******************************************************************************/
enum itxiaoyeLedIoc{
    itxiaoyeLedIocLedControl = 1,
    itxiaoyeLedIocMax
};


/*******************************************************************************
* DataType define
*******************************************************************************/
typedef enum {
    ENUM_ITXIAOYE_LED_POWER_OFF = 0, // 灭灯
    ENUM_ITXIAOYE_LED_POWER_ON       // 亮灯
}ItxiaoyePowerCtl_t;


/*******************************************************************************
* Global functions declaration
*******************************************************************************/
extern ReturnStatus_t ItxiaoyeLedInit(void);      // 灯设备初始化
extern ReturnStatus_t ItxiaoyeLedContr(ItxiaoyePowerCtl_t power);     // 灯设备控制
extern ReturnStatus_t ItxiaoyeLedExit(void);      // 灯设备退出

#endif // __ITXIAOYE_LED_H__