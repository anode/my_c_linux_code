/*
 * @Author: itxiaoye
 * @Date: 2022-07-24 18:04:59
 * @LastEditors: Anode you@example.com
 * @LastEditTime: 2022-08-10 22:46:35
 * @FilePath: /program/src/drives/itxiaoye_led/ItxiaoyeLed.c
 * @Description: led灯驱动程序
 */

/*****************************************************************************
* Include
*****************************************************************************/
#include <stdio.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include "ItxiaoyeLed.h"

/*******************************************************************************
* Macro
*******************************************************************************/


/*******************************************************************************
* Global variables
*******************************************************************************/
static int itxiaoyeLedFileHandle = -1;		// 灯设备文件句柄

/*******************************************************************************
* Local functions declaration
*******************************************************************************/

/**
 * @description: 灯设备初始化
 * @return {*} 初始化是否成功
 */
ReturnStatus_t ItxiaoyeLedInit(void){
    printf("init led \n");

	/* 打开驱动文件 */
	itxiaoyeLedFileHandle = open("/dev/itxiaoye_led",O_RDWR);
	if(itxiaoyeLedFileHandle<0)
	{
		printf("Can't open file!!!\r\n");
		return ENUM_STATUS_FAIL;
	}

	return ENUM_STATUS_SUCCESS;
}

/**
 * @description: 灯设备控制
 * @param {ItxiaoyePowerCtl_t} power 开关灯
 * @return {*} 控制是否成功
 */
ReturnStatus_t ItxiaoyeLedContr(ItxiaoyePowerCtl_t power){
	int ret;

    ret = ioctl(itxiaoyeLedFileHandle,ITXIAOYE_LED_IOC_LED_CONTR,&power); //写数据
	if(ret < 0)
	{
		printf("ioctl contrl error!!!");
		return ENUM_STATUS_FAIL;
	}
	printf("set power = %d \n",power);

	return ENUM_STATUS_SUCCESS;
}

/**
 * @description: 灯设备退出
 * @return {*} 退出是否成功
 */
ReturnStatus_t ItxiaoyeLedExit(void){
	int ret;

	/* 关闭设备 */
	ret = close(itxiaoyeLedFileHandle);
	if(ret < 0){
		printf("Can't close file!!!\r\n");
		return ENUM_STATUS_FAIL;
	}

	return ENUM_STATUS_SUCCESS;
}