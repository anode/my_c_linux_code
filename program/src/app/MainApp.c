/*
 * @Author: itxiaoye
 * @Date: 2022-07-24 15:25:25
 * @LastEditors: Anode you@example.com
 * @LastEditTime: 2022-08-16 22:09:18
 * @FilePath: /program/src/MainApp.c
 * @Description: 
 */

/*****************************************************************************
* Include
*****************************************************************************/
#include <stdio.h>
#include <unistd.h>
#include "ItxiaoyeLed.h"


/*******************************************************************************
* Macro
*******************************************************************************/


/*******************************************************************************
* Global variables
*******************************************************************************/


/*******************************************************************************
* Local functions declaration
*******************************************************************************/
void ItxiaoyeInit(void);  // 初始化操作
void ItxiaoyeExit(void);  // 退出操作
void LedTwinkle(uint32_t intervalTime);    // led闪烁


void main(){
    ItxiaoyeInit();
    
    LedTwinkle(100000);
    
    ItxiaoyeExit();
}

/**
 * @description: led闪烁
 * @param {uint32_t} intervalTime 间隔时间，us
 * @return {*}
 */
void LedTwinkle(uint32_t intervalTime){
    ItxiaoyePowerCtl_t ledPower =  ENUM_ITXIAOYE_LED_POWER_OFF;

    while(1){
        ItxiaoyeLedContr(ledPower);
        if(ledPower){
            ledPower = ENUM_ITXIAOYE_LED_POWER_OFF;
        }
        else{
            ledPower = ENUM_ITXIAOYE_LED_POWER_ON;
        }
        usleep(intervalTime);
    }
}

/**
 * @description: 初始化操作
 * @return {*}
 */
void ItxiaoyeInit(void){
    ItxiaoyeLedInit();
}

/**
 * @description: 退出操作
 * @return {*}
 */
void ItxiaoyeExit(void){
    ItxiaoyeLedExit();
}