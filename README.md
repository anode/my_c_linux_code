# my_c_linux_code

#### 介绍
自己平时在树莓派上练习，写的一些代码，主要是嵌入式linux相关的

#### 软件架构
1. code：内核驱动代码
2. dts：设备树相关修改
3. program：应用层代码

#### 使用说明

1.  查看博文中的《玩转树莓派》合集

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 其他

1.  我的博客：https://itxiaoye.top/
