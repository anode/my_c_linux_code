#ifndef __LED_H__

#define ITXIAOYE_LED_IOC_MAGIC  'l' // 定义幻数
#define ITXIAOYE_LED_IOC_LED_CONTR _IOW(ITXIAOYE_LED_IOC_MAGIC,itxiaoyeLedIocLedControl,int) // LED控制

enum itxiaoyeLedIoc{
    itxiaoyeLedIocLedControl = 1,
    itxiaoyeLedIocMax
};

#endif // __LED_H__